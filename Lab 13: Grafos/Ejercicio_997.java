public class Ejercicio_997 {
    public class Lab_997 {
        //997. Find the Town Judge

        public int findJudge(int n, int[][] trust) {

            int v[] =new int[n+1];
            for(int[] i:trust) {
                v[i[1]]++;
                v[i[0]]--;
            }
            for(int i=1;i<=n;i++) {
                if(v[i]==n-1)
                    return i;
            }
            return -1;
        }
    }
}
