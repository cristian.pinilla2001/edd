class ArbolB{
    NodoArbolB root;
    int t;

    //Constructor
    public ArbolB(int t) {
        this.t = t;
        root = new NodoArbolB(t);
    }

    public int buscarClaveMayor() {
        int claveMaxima = getClaveMayor(this.root);

        return claveMaxima;
    }

    private int getClaveMayor(NodoArbolB current) {
        if (current == null) {
            return 0;
        }
        while (!current.leaf) {
            current = current.child[current.n];
        }

        return claveMayorPorNodo(current);
    }

    private int claveMayorPorNodo(NodoArbolB current) {
        return current.key[current.n - 1];
    }

    public void mostrarClavesNodoMinimo() {
        NodoArbolB temp = buscarNodoMinimo(root);

        if (temp == null) {
            System.out.println("Sin minimo");
        } else {
            temp.imprimir();
        }
    }

    public NodoArbolB buscarNodoMinimo(NodoArbolB nodoActual) {
        if (root == null) {
            return null;
        }

        NodoArbolB aux = root;
        while (!aux.leaf) {
            aux = aux.child[0];
        }
        return aux;
    }

    public void buscarNodoPorClave(int num) {
        NodoArbolB temp = search(root, num);

        if (temp == null) {
            System.out.println("No se ha encontrado un nodo con el valor ingresado");
        } else {
            print(temp);
        }
    }

    //Search
    private NodoArbolB search(NodoArbolB actual, int key) {
        int i = 0;
        while (i < actual.n && key > actual.key[i]) {
            i++;
        }
        if (i < actual.n && key == actual.key[i]) {
            return actual;
        }
        if (actual.leaf) {
            return null;
        } else {
            return search(actual.child[i], key);
        }
    }

    public void insertar(int key) {
        NodoArbolB r = root;

        if (r.n == ((2 * t) - 1)) {
            NodoArbolB s = new NodoArbolB(t);
            root = s;
            s.leaf = false;
            s.n = 0;
            s.child[0] = r;
            split(s, 0, r);
            nonFullInsert(s, key);
        } else {
            nonFullInsert(r, key);
        }
        double x = utilizacion();
        System.out.println("El Porcentaje de utilizacion es " + x +"%");
    }

    private void split(NodoArbolB x, int i, NodoArbolB y) {
        NodoArbolB z = new NodoArbolB(t);
        z.leaf = y.leaf;
        z.n = (t - 1);
        for (int j = 0; j < (t - 1); j++) {
            z.key[j] = y.key[(j + t)];
        }

        if (!y.leaf) {
            for (int k = 0; k < t; k++) {
                z.child[k] = y.child[(k + t)];
            }
        }

        y.n = (t - 1);
 

        for (int j = x.n; j > i; j--) {
            x.child[(j + 1)] = x.child[j];
        }                                    
        x.child[(i + 1)] = z;                                                   
        for (int j = x.n; j > i; j--) {
            x.key[(j + 1)] = x.key[j];
        }
        x.key[i] = y.key[(t - 1)];
        x.n++;
    }

    private void nonFullInsert(NodoArbolB x, int key) {
        if (x.leaf) {
            int i = x.n;
            while (i >= 1 && key < x.key[i - 1]) {
                x.key[i] = x.key[i - 1];
                i--;
            }

            x.key[i] = key;
            x.n++;
        } else {
            int j = 0;
            while (j < x.n && key > x.key[j]) {
                j++;
            }
            if (x.child[j].n == (2 * t - 1)) {
                split(x, j, x.child[j]);

                if (key > x.key[j]) {
                    j++;
                }
            }

            nonFullInsert(x.child[j], key);
        }
    }

    public void showBTree() {
        print(root);
    }

    private void print(NodoArbolB n) {
        n.imprimir();

        if (!n.leaf) {
            for (int j = 0; j <= n.n; j++) {
                if (n.child[j] != null) {
                    System.out.println();
                    print(n.child[j]);
                }
            }
        }
    }
    public int size(){
        return tamaño(root);
    }

    private int tamaño(NodoArbolB x){
        int valor = 0;
        for(int j=0; j<x.n; j++){
            valor++;
        }
       if(!x.leaf){
            for(int i=0; i<=x.n; i++){
                if(x.child[i]!=null){
                    valor+=tamaño(x.child[i]);
                }
            }
        }
        return valor;
    }
   public double utilizacion(){
        double valor = (double) size()/nodos(root);
        return valor*100;
    }
    private int nodos(NodoArbolB x){
        int valor = x.key.length;
        if(!x.leaf){
            for(int i=0; i<=x.n; i++){
                if(x.child[i]!=null){
                    valor+=nodos(x.child[i]);
                }
            }
        }
        return valor;
    }

}
