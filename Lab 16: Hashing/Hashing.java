public class Hashing {

    class Nodo{
        int key;
        public Nodo(int k) {
            key = k;
        }
    }

    Nodo [] T;

    public Hashing(int m) {
        T = new Nodo[m];
        for(int i = 0; i < m; i++) T[i] = null;
    }

   public boolean insertar(int key) {
        int i =0, j;
        Nodo x = new Nodo(key);
        while(i!=T.length){
            j= h(key,i);
            if(T[j]==null){
                T[j] = x;
                return true;
            }else{
                i++;
            }
        }
        return false;
    }

    public Nodo buscar(int key) {
        int i=0,j=0;
        while(i!=T.length){
            j=h(key,i);
            if(T[j]==null || T[j].key!=key){
                i++;
            }else if(T[j].key==key){
                return T[j];
            }
        }
        return null;
    }

    public boolean eliminar(int key) {
        int j=0;
        for(int i=0; i<T.length; i++){
            j=h(key,i);
            Nodo x = T[j];
            if(key == x.key){
                T[j]=null;
                return true;
            }
        }
        return false;
    }
    public double chargeFactor(){
        int cant = 0;
        for(int i=0; i<T.length; i++){
            if(T[i]!=null){
                cant++;
            }
        }
        return cant/T.length;
    }

    int h(int k, int i) {
        return (h1(k) + (i * h2(k))) % T.length;
    }

    int h1(int k) {
        return k % T.length;
    }

    int h2(int k) {
        return 1 + (k % (T.length-1));
    }

    /*
3. Responda las siguientes preguntas como comentario en su código:
A) ¿Qué ocurre con las funciones de inserción, búsqueda y eliminación cuando el factor de carga es cercano a 1?
Respuesta:
    "Para la función de inserción, el tiempo de retraso aumentará a medida que aumente el factor porque
El método tomaría más para encontrar un espacio vacío para el nodo. La función de investigación también aumentará, porque
El método llevará más tiempo si hay más espacios utilizados con números similares, pero la función de eliminación continuará siendo la misma,
Como independientemente del número de nodos utilizados, buscara por completo el hashing para eliminar la clave solicitada.

B) Describa los pasos a realizar para duplicar el tamaño de la tabla Hash y reducir el factor de carga. ¿Para qué
valor de 𝛼 es necesario realizar este proceso?
Respuesta- 
    "Los pasos a realizar serían: 
        Primero, En el método de inserción se verificaría si el factor de carga es 1 después
de Agreguar el último nodo, si es cierto, dirijira el método de duplicación de la tabla hash, donde se creara un arreglo 
para copiar los datos del hashing y el arreglo T se recrearía en un tamaño doble, para finalmente agregar
Por el mismo método de inserción con cada llave almacenada en los nodos del arreglo de copia. Es por eso que el valor de
𝛼 Debería ser exactamente 1, porque implicaría  que el hash esta lleno y necesitaría más espacio para reducir
el valor."
 */

}
