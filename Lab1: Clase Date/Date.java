package Lab_Date;

public class Date {
    
    private int date;
    
    public Date(int date) {
        if(date!=0){this.date = date;}
        this.date = 0;
    }
    public int getAnio() {
        int a=date>>>20;
        return a;
    }

    public int getMes() {
        int a=date<<12;
        a=a>>>28;
        return a;
    }

    public int getDia() {
        int a=date<<16;
        a=a>>>27;
        return a;
    }

    public void setAnio(int anio) {
        if(anio<=4095&&anio>0){
            int maskBor=~(4095<<20);
            this.date=date& maskBor;
            int mask=anio;
            mask=mask<<20;
            this.date=this.date|mask;
        }
    }

    public void setMes(int mes) {
        if(mes<=12&&mes>0){int mask=mes;
            int maskBor=~(15<<16);
            this.date=date& maskBor;
            mask=mask<<16;
            this.date=this.date|mask;
        }
    }

    public void setDia(int dia) {
        if(dia<=31&&dia>0){int mask=dia;
            int maskBor=~(31<<11);
            this.date=date& maskBor;
            mask=mask<<11;
            this.date=this.date|mask;
        }
    }

    @Override
    public String toString() {
        return "Date: " + getDia()+"/"+getMes()+"/"+getAnio();
    }
    
    
}
