class Lista { 
 private Nodo laCabeza;
 Lista() { 
    laCabeza = null;
 } 
 public void InsertaInicio(int o) {
  if (EstaVacia()) laCabeza=new Nodo(o, null);
  else  laCabeza = new Nodo(o, laCabeza);
}

public void InsertaFinal(int o) {
  if (EstaVacia()) laCabeza=new Nodo(o, null);
   else{
         Nodo t;
         for(t = laCabeza; t.next != null; t= t.next) ;
         t.next = new Nodo(o,null);
     }
 }

    public int[] getInt() {
        Nodo t=laCabeza;
        int prom=RetornaPromedio();
        int[] listaPrev=new int[Size()];
        for (int i = 0; i < Size(); i++) {
            if(t.elObjeto>prom){
                listaPrev[i]=t.elObjeto;
                
            }
            t=t.next;
        }
        
        
        return listaPrev;
    }

  
public int Size() {
 int tnodos=0;
 for(Nodo t = laCabeza; t !=null; t= t.next)  tnodos++;
 return tnodos;
}


public void Eliminar(int o) { 
 if(!EstaVacia()) { 
  if(laCabeza.elObjeto==o) laCabeza = laCabeza.next;
   else { 
     Nodo p = laCabeza;
     Nodo t = laCabeza.next; 
     while (t !=null && t.elObjeto != o)  { 
      p = t ; t = t.next;
     } 
     if(t.elObjeto==o) p.next = t.next;
    }
 }
}

public boolean EstaVacia() {
return laCabeza == null;
}

void Print() {
   if(laCabeza!=null) Imprimir(laCabeza);
    else System.out.println("Lista Vacia");
    }

 void Imprimir(Nodo m ) {
    if(m !=null) {m.Print(); Imprimir(m.next);}
  }

public int RetornaPromedio(){
    int p=0;
    int cant=0;
    if(EstaVacia()==true)return p;
    for(Nodo t = laCabeza; t !=null; t= t.next) {
        p=p+t.elObjeto;       
        cant++;
    }
    
    return p/cant;
}
 
 private class Nodo { 
  public int elObjeto;
  public Nodo next;
  public Nodo(int nuevoObjeto, Nodo next)
  {this.elObjeto=nuevoObjeto; this.next = next;} 
  void Print(){ System.out.print("- " + elObjeto);}
 }

}
