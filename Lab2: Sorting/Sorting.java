package Lab_Sorting;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Sorting {

	public static int[] fillArray(int n) {
		int[] array = new int[n];
		Random rnd = new Random();
		for (int i = 0; i < array.length; i++) {
			array[i] = rnd.nextInt(n * 10);
		}
		return array;
	}

	public static void bubbleSort(int array[]) {
		int n = array.length;
		for (int i = 0; i < n - 1; i++) {
			for (int j = 0; j < n - i - 1; j++) {
				if (array[j] > array[j + 1]) {
					// swap arr[j+1] and arr[j]
					int temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
				}
			}
		}
	}

	public static void merge(int array[], int l, int m, int r) {

		int n1 = m - l + 1;
		int n2 = r - m;
		int L[] = new int[n1];
		int R[] = new int[n2];

		for (int i = 0; i < n1; ++i)
			L[i] = array[l + i];
		for (int j = 0; j < n2; ++j)
			R[j] = array[m + 1 + j];

		int i = 0, j = 0;


		int k = l;
		while (i < n1 && j < n2) {
			if (L[i] <= R[j]) {
				array[k] = L[i];
				i++;
			} else {
				array[k] = R[j];
				j++;
			}
			k++;
		}

		while (i < n1) {
			array[k] = L[i];
			i++;
			k++;
		}

		while (j < n2) {
			array[k] = R[j];
			j++;
			k++;
		}
	}

	public static void sort(int array[], int l, int r) {
		if (l < r) {

			int m = l + (r - l) / 2;


			sort(array, l, m);
			sort(array, m + 1, r);


			merge(array, l, m, r);
		}
	}
	
	public static void mergeSort(int array[]) {
		sort(array, 0, array.length - 1);
	}
	
	public static void print(int array[]) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}
	
	
	public static void main(String[] args) {
		
		
		long data[][] = new long[9][3];
		
                int i=0;
                int n=0;
               
                switch(i+1) {
                    case 1: n=1000;break;
                    case 2: n=5000;break;
                    case 3: n=10000;break;
                    case 4: n=50000;break;
                    case 5: n=100000;break;
                    
                }
                
                for (; i < 5; i++) {
                
            
		int arr1[] = fillArray(n);
		int arr2[] = fillArray(n);
		
		int arr1_1[] = arr1.clone();
		int arr2_1[] = arr2.clone();
		int arr1_2[] = arr1.clone();
		int arr2_2[] = arr2.clone();
		
		long startTime = System.nanoTime();
		bubbleSort(arr1);
		long finishTime = System.nanoTime() - startTime;
		data[i][0] = finishTime;
		
		startTime = System.nanoTime();
		bubbleSort(arr2);
		finishTime = System.nanoTime() - startTime;
		data[i][0] = (data[0][0]+finishTime) / 2;
		
		startTime = System.nanoTime();
		mergeSort(arr1_1);
		finishTime = System.nanoTime() - startTime;
		data[i][1] = finishTime;
		
		startTime = System.nanoTime();
		mergeSort(arr2_1);
		finishTime = System.nanoTime() - startTime;
		data[i][1] = (data[0][1]+finishTime) / 2;
		
		startTime = System.nanoTime();
		Arrays.sort(arr1_2);
		finishTime = System.nanoTime() - startTime;
		data[i][2] = finishTime;
		
		startTime = System.nanoTime();
		Arrays.sort(arr2_2);
		finishTime = System.nanoTime() - startTime;
		data[i][2] = (data[0][2]+finishTime) / 2;
		
		
            }
            for (int j = 0; j < 9; j++) {
                for (int k = 0; k < 3; k++) {
                    System.out.print((data[j][k]/1000)+"\t");
                }
                System.out.println("");
            }
    }
}
